import express from 'express';
import morgan from 'morgan';
import path from 'path';
import { readFile } from 'fs';

const app = express()
const dir = process.cwd();

app.use(morgan('tiny'))
app.use(express.static('public'))

readFile('package.json', 'utf8', (err, data) => {
  if (err) throw err
  const { name, version } = JSON.parse(data)
  const port = process.env.PORT || 3000;
  app.listen(port, () => {
    console.debug(`${name} v${version} listening on ${port}...`)
  })
})
