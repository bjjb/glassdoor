# GlassDoor

## A Markdown slide-show renderer

---
# A Heading

A paragraph immediately following a heading.

---
A paragraph alone on a page.

---
Several paragraphs.

All alone on the page.

Alone.

---
# Code blocks

A page with some code (in a `<pre>`).

```
I am some code, formatted within a <pre>
(and with a <code> therein).
Aren't I grand.

Read me.
```

The code should stand out, but not be too wide.

---
# Pictures

Pictures should look nice:

![A picture](https://picsum.photos/400/300 "Check this out!")

---
# Floating pictures

You can float them right, like this:
![A right-floated picture →](https://picsum.photos/200 "I float right")
(the picture should float to the right of the text.)

Or left, like this:
(this picture should be floating to the left).
![A left-floated picture ←](https://picsum.photos/200 "I float left")

---
