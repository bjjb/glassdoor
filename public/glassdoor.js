import './marked.js'

/**
 * Splits an array into multiple arrays on matching items.
 */
const split = (items, match) => {
  const results = []
  let result = []
  Array.from(items).forEach((item) => {
    if (match(item)) {
      results.push(result)
      result = []
      return
    }
    result.push(item)
  })
  results.push(result)
  return results
}

/**
 * Returns a function with the signature `f(html) -> HTMLElement`, which
 * will build a HTMLElement with the given innerHTML.
 */
const render = (name) => {
  return (html) => {
    const element = document.createElement(name)
    element.innerHTML = html
    return element
  }
}

/**
 * Returns a function with the signature `f(elements, index)` which will
 * make a an element of type `type` containing the given elements, and with an
 * ID of `slide-${index}`.
 */
const slide = (name) => {
  return (elements, index) => {
    const container = document.createElement(name)
    container.dataset.index = index
    container.id = `slide-${index}`
    container.append(...elements)
    return container
  }
}

/*
 * A GlassDoor custom element reads its src attribute URL as markdown and
 * converts it to HTML, with horizontal rules indicating delimiters, and all
 * delimited sections in <section> elements within a <article> element (within
 * the shadow DOM). Its css attribute is added as the href to a <link> in the
 * shadow DOM.
 */
class GlassDoor extends HTMLElement {
  /**
   * Builds a new GlassDoor.
   */
  constructor() {
    super()
    // const mode = 'open';
    // const delegatesFocus = true;
    // this.root = this.attachShadow({ mode, delegatesFocus })
    this.root = this
    this.article = document.createElement('article')
    this.css = document.createElement('link')
    this.css.rel = 'stylesheet'
    this.root.append(this.article, this.css)
    addEventListener('load', this)
    addEventListener('keydown', this)
    addEventListener('dragover', this)
    addEventListener('dragstart', this)
    addEventListener('dragenter', this)
    addEventListener('dragleave', this)
    addEventListener('drop', this)
  }

  static get observedAttributes() {
    return ['src', 'css']
  }

  get index() {
    return parseInt(location.hash.split('-')[1]) || 0
  }

  get length() {
    return this.root.querySelectorAll('section').length
  }

  get slides() {
    return Array.from(this.article.querySelectorAll('section'))
  }

  get exposed() {
    return Object.hasOwnPropertyName(this.article.classList, 'exposed')
  }

  connectedCallback(event) {
    console.debug('%o connectedCallback(%o)', this, event)
  }

  attributeChangedCallback(name, oldValue, newValue) {
    switch (name) {
      case 'src':
        return this.reload()
        break
      case 'css':
        return this.reload()
        break
      default:
        throw new Error(`unexpected attributeChangedCallback: ${name}`)
    }
  }

  reload() {
    if (this.hasAttribute('css')) {
      this.css.href = this.getAttribute('css')
    }
    if (this.hasAttribute('src')) {
      fetch(this.getAttribute('src'))
        .then(r => r.text())
        .then(markdown => marked(markdown))
        .then(render('template'))
        .then(t => t.content)
        .then(t => split(t.children, e => e.nodeName === 'HR'))
        .then(contents => contents.map(slide('section')))
        .then(slides => this.article.replaceChildren(...slides))
        .then(() => this.show(this.index))
    }
  }

  /**
   * Shows the slide at the given index (if it's within bounds).
   * It'll hide all the others. It also updates the location hash.
   */
  show(index) {
    if (index >= 0 && index < this.length) {
      this.slides.forEach((slide, i) => {
        slide.hidden = (i !== index)
      })
      location.hash = `#slide-${index}`
    }
  }

  first() {
    this.show(0)
  }

  next() {
    this.show(this.index + 1)
  }

  prev() {
    this.show(this.index - 1)
  }

  last() {
    this.show(this.length - 1)
  }

  expose() {
    this.article.classList.toggle('exposed')
  }

  dragenter() {
    this.article.classList.add('dropready')
  }

  dragleave() {
    this.article.classList.remove('dropready')
  }

  drop(...files) {
    const r = new FileReader()
    r.addEventListener('loadend', this)
    this.article.classList.remove('dropready')
    files.forEach(file => r.readAsBinaryString(file))
  }

  fullscreen() {
    if (this.article.requestFullscreen) {
      this.article.requestFullscreen()
      return // good browser
    }
    if (this.article.webkitRequestFullscreen) {
      this.article.webkitRequestFullscreen()
      return // less good browser
    }
    console.error(`this browser doesn't support requestFullscreen`)
  }

  handleEvent(event) {
    switch (event.type) {
      case 'dragenter': // dragged object entered the zone
        event.preventDefault()
        return this.dragenter()
      case 'dragleave': // dragged object left the zone
        event.preventDefault()
        return this.dragleave()
      case 'dragover': // drag within the zone (important!)
        event.preventDefault()
        return
      case 'drop': // object dropped in the zone
        event.preventDefault()
        return this.drop(...event.dataTransfer.files)
      case 'loadend': // file-reader finished
        const content = event.target.result
        const encoded = btoa(content)
        const src = `data:text/plain;base64,${encoded}`
        const u = new URL(location)
        this.setAttribute('src', src)
        u.searchParams.set('src', src)
        location.assign(u)
        return
      case 'load': // page loaded
        const url = new URL(location)
        const { searchParams } = url
        Array('src', 'css').forEach((s) => {
          if (searchParams.has(s) && searchParams.get(s)) {
            this.setAttribute(s, searchParams.get(s))
          }
        })
        break
      case 'keydown': // key pressed
        switch (event.key) {
          case ' ':
          case 'PageDown':
          case 'ArrowRight':
            event.preventDefault()
            return this.next()
          case 'PageUp':
          case 'ArrowLeft':
            event.preventDefault()
            return this.prev()
          case 'End':
            event.preventDefault()
            return this.last()
          case 'Home':
            event.preventDefault()
            return this.first()
          case 'Escape':
            event.preventDefault()
            return this.expose()
          case 'f':
          case 'F':
            event.preventDefault()
            return this.fullscreen()
        }
        break
      default:
        throw new Error(`unexpected event ${event.type}`)
    }
  }

  static get style() {
    const tmpl = document.createElement('template')
    tmpl.innerHTML = `<style>
      /**
       * The basic style for a slide-deck. The markup should consist of a
       * article element with section children, each of which is a single
       * slide.
       */
    </style>`
    return tmpl.content.cloneNode(true)
  }
}

export default GlassDoor
