# GLASSDooR

## GitLab Automatic SlideShow Deck Renderer

A small JS library and web-app for showing Markdown-sources slideshows.

Inspired by [mark.show][1], (which uses [Reveal.js][2]), but more CSS-driven
and suitable for internal use (specifically GitLab hosted snippets, wiki
pages, etc).

## Usage

Write your slides in [Markdown][3], separating pages with [horizontal
rules][4]. For example:

```md
# My title

Welcome to *my* slideshow.

![Picture](https://picsum.photos/200/300)

---

* [x] Write markdown
* [x] Use [glassdoor](https://bjjb.gitlab.io/glassdoor)
* [ ] Answer questions
```

Now drag that file onto the page.

It uses [marked.js][5] under the hood. The basic presentation styling is
built-in, but you can include additional styling with `?css`
options to the input (or by dropping a CSS file onto the page).

[1]: https://mark.show/
[2]: https://revealjs.com/
[3]: https://spec-md.com/
[4]: https://spec-md.com/#sec-Horizontal-Rules
[5]: https://marked.js.org
